# Trouver 24 — exploration

- English version, followed by french one.
- Version anglaise suivie de la française.

## Synopsis

This script helps in exploration of almost all solutions of
[Find 24](https://lehollandaisvolant.net/tout/tools/24/).
Examples bellow.

Ce script permet d’explorer quasiment toutes les solutions de
[Trouver 24](https://lehollandaisvolant.net/tout/tools/24/).
Exemples ci-dessous.

```console
$ # version 1.0
$ ./trouve24.sh 1 2 3 8
1/2=0; 3*8=24; 0+24=24
1/2=0; 8*3=24; 0+24=24
1+3=4; 8-2=6; 4*6=24
2-1=1; 3*8=24; 1*24=24
2-1=1; 8*3=24; 1*24=24
3+1=4; 8-2=6; 4*6=24
2-1=1; 3*1=3; 3*8=24
2-1=1; 3/1=3; 3*8=24
3*8=24; 1/2=0; 24+0=24
3*8=24; 1/2=0; 24-0=24
3*8=24; 2-1=1; 24*1=24
3*8=24; 2-1=1; 24/1=24
8-2=6; 1+3=4; 6*4=24
2-1=1; 8*1=8; 8*3=24
2-1=1; 8/1=8; 8*3=24
8-2=6; 3+1=4; 6*4=24
8*3=24; 1/2=0; 24+0=24
8*3=24; 1/2=0; 24-0=24
8*3=24; 2-1=1; 24*1=24
8*3=24; 2-1=1; 24/1=24
20 solutions for 2832 computed.
$ # version 2.1
$ ./trouve24.sh 7 7 1 2
2/7=.28; 7/.28=25.00; 25.00-1=24; B1
7*7=49; 49-1=48; 48/2=24; B3
7*7=49; 49-1=48; 48/2=24; L1
7*7=49; 49-1=48; 48/2=24; L2
2/7=.28; 7/.28=25.00; 25.00-1=24; B1
7*7=49; 49-1=48; 48/2=24; B3
7*7=49; 49-1=48; 48/2=24; L1
7*7=49; 49-1=48; 48/2=24; L2
Found 8 solutions for 10648 computations.
$ # version 3.2
$ ./trouve24.sh 8 8 1 11
1 * 11 = 11;	11 - 8 = 3;	3 * 8 = 24;	
1 * 11 = 11;	11 - 8 = 3;	3 * 8 = 24;	
11 - 8 = 3;	3 * 8 = 24;	24 * 1 = 24;	
11 - 8 = 3;	3 * 8 = 24;	24 / 1 = 24;	
11 - 8 = 3;	3 * 1 = 3;	3 * 8 = 24;	
11 - 8 = 3;	3 / 1 = 3;	3 * 8 = 24;	
11 - 8 = 3;	3 * 8 = 24;	24 * 1 = 24;	
11 - 8 = 3;	3 * 8 = 24;	24 / 1 = 24;	
11 - 8 = 3;	3 * 1 = 3;	3 * 8 = 24;	
11 - 8 = 3;	3 / 1 = 3;	3 * 8 = 24;	
11 * 1 = 11;	11 - 8 = 3;	3 * 8 = 24;	
11 / 1 = 11;	11 - 8 = 3;	3 * 8 = 24;	
11 * 1 = 11;	11 - 8 = 3;	3 * 8 = 24;	
11 / 1 = 11;	11 - 8 = 3;	3 * 8 = 24;	
Found 14 solutions over 1536 computations.
```

## Arguments

This script currently goes further than my
[initial POC](https://framagit.org/-/snippets/6519).

Le présent script va plus loin que mon
[premier jet](https://framagit.org/-/snippets/6519),
et prend en compte tous les longs [échanges sur
LinuxFr](https://linuxfr.org/users/steph1978/journaux/resoudre-trouve-24).

| option  | purpose                   | objectif                         |         |
| ------  | ------------------------- | -------------------------------- | ------- |
| `-t`N   | solve for N instead of 24 | résoudre pour N au lieu de 24    | total   |
| `-v`N   | solve for N instead of 24 | résoudre pour N au lieu de 24    | value   |
| `-s`N   | use N decimal precision   | travaille en précision de N      | scale   |
| `-f`    | exit on first solution    | arrêt à la première solution     | first   |
| `-e` ID | explore only solutions ID | explorer que les solutions ID    | explore |
| `-b` ID | explore only solutions ID | explorer que les solutions ID    | branch  |
| `-u`    | try to avoid duplications | essayer de retirer les doublons  | unique  |
| `-h`    | show this help and exit   | afficher cette aide et finir     | help    |
| `-d`    | show ongoing explorations | montrer les exploration en cours | debug   |
| `-q`    | do not show any result    | ne pas montrer de résultat       | quiet   |

Exploration paths, given to `-e` or `-b`, are named with two case insensitive
characters easy to remember (I hope.) Many can be selected with use of coma
as separator.

Les voies d’exploration, indiquées à `-e` ou `-b`, sont composées de deux
caractères faciles à se rappeler (du moins je l’espère, bien que ce soit
en anglais.) On peut en indiquer plusieurs en les séparant par la virgule.

| ID   | solution type | note                             |
| ---- | ------------- | -------------------------------- |
| `b1` | `Ax(ByC)zD`   | Balanced parentheses 1           |
| `b2` | `(AxB)y(CzD)` | Balanced parentheses 2           |
| `b3` | `AxByCzD`     | Balanced parentheses 3 (default) |
| `l1` | `(AxB)yCzD`   | Left sided parentheses 1         |
| `l2` | `(AxByC)zD`   | Left sided parentheses 2         |
| `r1` | `AxBy(CzD)`   | Right sided parentheses 1        |
| `r2` | `Ax(ByCzD)`   | Right sided parentheses 2        |

