# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

They’re two R2 solutions here:
```
4÷ (1- 5÷6 )
6÷ ( 5÷4 -1)
```
but only one (the latest) is find by default.

## [3.3.0] — 2022-03-04

Pretty and sexy

### Changed

- pretty print steps with Unicode mathematical symbols
- pretty print integers by avoiding extra zeros in decimal part

### Fixed

- correct important bug: some solutions are skipped when changing scale
- correct exploration list option handling

## [3.2.0] — 2022-03-02

Nice youth

### Added

- second level of uniqueness tracking

### Changed

- more code cleaning and refactor

### Fixed

- integers arguments check

## [3.1.0] — 2022-03-02

Smarter child

### Added

- option `-u` to avoid duplications
- option `-h` to list available switches

### Fixed

- correct options parsing

## [3.0.0] — 2022-03-02

Age of reason

### Changed

- no longer show solution id when there's only one
- respect singular and plural on final summary
- restrict to one branch/exploration by default

## [2.3.0] — 2022-03-01

A bit more fun

### Added

- option `-f` for fast exit

### Changed

- permutations are programmatically generated instead of being hard coded
- the four operations are aligned

## [2.2.0] — 2022-02-28

The more the merrier

### Added

- option `-t` for other puzzles
- option `-s0` for integers arithmetic like version 1.x
- option `-d` to show on going exploration
- option `-e` to select exploration branch

## [2.1.1] — 2022-02-27

### Fixed

- just sort functions by name

## [2.1.0] — 2022-02-27

Deeper exploration.

### Changed

- code reorganisation
  ⇒ slightly less `dc` and more `tr`
- more results (sometimes surprising)

### Fixed

- rework on guards
- miscellaneous corrections

## [2.0.1] — 2022-02-27

### Fixed

- emergency fix on negative numbers

## [2.0.0] — 2022-02-26

New computation engine for more solutions, but a bit slower.

### Added

- new dependencies
- more solutions (id=b1 & id=b2) found, e.g. for: `1 4 5 6`

### Changed

- replace shell arithmetic on _Z_ with `dc` arithmetic on _D_
  ⇒ but the script takes longer time.

### Fixed

- dependencies check
- other corrections

## [1.2.0] — 2022-02-25

Refinement and polishing.

### Added

- solutions `AxByCzD` (id=b3)

### Changed

- internal functions rename
- cleanup by intermediate variables removal
- solutions ids renamed:
  | old | new | solution type |
  | --- | --- | ------------- |
  | 2   | b1  | `Ax(ByC)zD`   |
  | 1   | b2  | `(AxB)y(CzD)` |
  | ?   | b3  | `AxByCzD`     |
  | 3   | l1  | `(AxB)yCzD`   |
  | 5   | l2  | `(AxByC)zD`   |
  | 4   | r1  | `AxBy(CzD)`   |
  | 6   | r2  | `Ax(ByCzD)`   |

### Fixed

- remove not accepted solutions (division on _D_ instead of _N_)

## [1.1.0] — 2022-02-25

More solutions.

### Added

- solutions `AxBy(CzD)` (id=4)
- solutions `(AxB)yCzD` (id=3)
- solutions `Ax(ByCzD)` (id=6)
- solutions `(AxByC)zD` (id=5)
- comments for internal functions

### Changed

- better output with ids
- solutions ids numbered:
  | id | solution type |
  |----| ------------- |
  | 2  | `Ax(ByC)zD`   |
  | 1  | `(AxB)y(CzD)` |
  | ?  | `AxByCzD`     |
  | 3  | `(AxB)yCzD`   |
  | 5  | `(AxByC)zD`   |
  | 4  | `AxBy(CzD)`   |
  | 6  | `Ax(ByCzD)`   |

## [1.0.0] — 2022-02-24

First and obvious solutions.

### Added

- solutions `(AxB)y(CzD)`
- solutions `Ax(ByC)zD`
