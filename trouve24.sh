#!/bin/sh
# ex: ai:sw=4:ts=4
# vim: ai:ft=sh:sw=4:ts=4:ff=unix:sts=4:et:fenc=utf8
# -*- sh; c-basic-offset: 4; indent-tabs-mode: nil; tab-width: 4;
# atom: set usesofttabs tabLength=4 encoding=utf-8 lineEnding=lf grammar=shell;
# mode: shell; tabsoft; tab:4; encoding: utf-8; coding: utf-8;
##########################################################################
# $1: integer
# $2: integer
# $3: integer
# $4: integer

for c in 'dc' 'grep' 'test' 'tr'
do
    if ! command -v "$c" >/dev/null
    then
        echo "Error: command $c is not found" >&2
        exit 3
    fi
done

_explo='b3' # string: as coma-separated items list 'b1,b2,b3,l1,l2,r1,r2'
_debug=0  # boolean: 0 is false/unset, not 0 is true/set
_first=0  # boolean: 0 is false/unset, not 0 is true/set
_uniqs=0  # boolean: 0 is false/unset, not 0 is true/set
_quiet=0  # boolean: 0 is false/unset, not 0 is true/set
_scale=2  # integer: greater or equal to zero
_value=24 # integer: greater or equal to nine
_usage="Usage: $0 [opts] int int int int" # string for internal use: synopsys
_inapi="is not a positive integer" # string for internal use: type error mesg
while getopts ':b:cde:fhk:oqs:t:uv:' o
do
    case $o in
        b|e) # Branches to Explore
            if echo "$OPTARG" | grep -Eiqsv '([blr][12]|b3)(,([blr][12]|b3))*'
            then
                echo "$0: $OPTARG is an invalid list" >&2
                exit 1
            else
                _explo="$OPTARG"
            fi
            ;;
        f|o) # First result, only One result
            _first=1
            ;;
        d) # Debug mode
            _debug=1
            ;;
        h)
            echo "$_usage"
            echo ''
            echo " opts:" # following list, sorted alphabetically
            echo "  -d: verbose mode on, see also -q for debug"
            echo "  -e list: select alternative exploration paths"
            echo "  -h: show this help/infos screen then exit"
            echo "  -o: show only one of the possible solutions"
            echo "  -q: don't show results, see also -d for debug"
            echo "  -s 0: compute only on integers and no decimal"
            echo "  -u: try to avoid showing some repeated solutions"
            echo "  -v int: set target to int instead of default 24"
            exit 0
            ;;
        q) # Quiet
            _quiet=1
            ;;
        k|s) # Scale
            case "$OPTARG" in
                *[!0-9]*|'')
                    echo "$0: $OPTARG $_inapi" >&2
                    exit 1
                    ;;
                *)
                    _scale=$(( OPTARG ))
                    ;;
            esac
            ;;
        t|v) # Target Value
            case "$OPTARG" in
                *[!0-9]*|'')
                    echo "$0: $OPTARG is not a positive integer" >&2
                    exit 1
                    ;;
                *)
                    if test $OPTARG -lt 10
                    then
                        echo "$0: $OPTARG is not greater than 9" >&2
                        exit 1
                    fi
                    _value=$(( OPTARG ))
                    ;;
            esac
            ;;
        u|c) # use likely Combinaisons or Unique permutations
            _uniqs=$(( _uniqs + 1 ))
            ;;
        :)
            echo "$0: Argument required for -$OPTARG." >&2
            exit 1
            ;;
        ?)
            echo "$0: Invalid option -$OPTARG." >&2
            exit 1
            ;;
    esac
done
shift $(( OPTIND - 1 ))

if test $# -lt 4
then
    echo "$_usage" >&2
    exit 1
fi
unset _usage # not needed anymore

for i in "$1" "$2" "$3" "$4"
do
    if echo "$i" | grep -Eqsv '^ *[0-9]+$'
    then
        echo "Error: $i $_inapi" >&2
        exit 2
    fi
done
unset _inapi # not needed anymore

_check=0 # integer for internal use: number of computations
_found=0 # integer for internal use: number of solutions

# $1 is computed expression
# $2 is module/branch name
show_debug() {
    _check=$(( _check + 1 ))
    test $(( _debug )) -ne 0 &&
        echo "DeBuG:::${2:-??}::$( echo "$1" | tr -su ' */' ' ×÷' )" >&2
}

# $1 is first expression
# $2 is first result
# $3 is second expression
# $4 is second result
# $5 is third expression
# $6 is exploration branch info
#    third result should be $_value
show_result() {
    _found=$(( _found + 1 ))
    test $(( _quiet )) -eq 0 &&
        printf '%s = %g;\t%s = %g;\t%s = %i;\t%s\n' \
               "$1" "$2" "$3" "$4" "$5" "$_value" \
               "$( echo "$6" | grep -iqs "$_explo" &&
                    echo '' || echo "$6" )"
    return $(( _first ))
}

# $1 is a signed integer
#    e.g. "-7" or "+8" or "9"
# note: use of 'tr' to ensure proper negative integers for 'dc'
#       because parameter expansion "${var/patern/replace}" isn't POSIX
#       http://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_06_02
norm_int() {
    printf '%s' "${1:-0}" | tr -u '-' '_' | tr -du '+*/'
}

# type: Balanced parentheses 1
# form: A  x  (B  y  C)  z  D
# args: $3 $1 ($4 $o $5) $2 $6
# example: 3*(2-1)*8
explore_b1() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    test "$2" = '/' && test "$6" -eq 0 && return # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$o" = '/' && test "$5" -eq 0 && continue # shouldn't happen
        f=$( printf '%g' "$( dc -e "$_scale k$4 $5$o p" )" )
        test "$1" = '/' && test "$f" = 0 && continue
        show_debug "$3$1($4$o$5)$2$6" 'B1'
        s=$( printf '%g' "$( dc -e "$_scale k$3 $( norm_int "$f" )$1p" )" )
        dc -e "$_scale k$( norm_int "$s" ) $6$2p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$4 $o $5" "$f" "$3 $1 $f" "$s" "$s $2 $6" 'B1' || break 9
    done
}

# type: Balanced parentheses 2
# form: (A  x  B)  y  (C  z  D)
# args: ($3 $1 $4) $o ($5 $2 $6)
# example: (8-5)*(7+1)
explore_b2() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    test "$2" = '/' && test "$6" -eq 0 && return # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$2$o" = '//' && test $5 -lt $6 && continue
        s=$( dc -e "$_scale k$5 $6$2p" 2>/dev/null )
        test "$o" = '/' && test "${s:-0}" = 0 && continue
        show_debug "($3$1$4)$o($5$2$6)" 'B2'
        f=$( printf '%g' "$( dc -e "$_scale k$3 $4$1p" )" )
        dc -e "$_scale k$( norm_int "$f" ) $( norm_int "$s" )$o p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$3 $1 $4" "$f" "$5 $2 $6" "$s" "$f $o $(printf '%g' $s)" 'B2' || break 9
    done
}

# type: Balanced parentheses 3
# form: A  x  B  y  C  z  D
# args: $3 $1 $4 $2 $5 $o $6
# example:
explore_b3() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    test "$2" = '/' && test "$5" -eq 0 && return # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$o" = '/' && test "$6" -eq 0 && continue # shouldn't happen
        show_debug "$3$1$4$2$5$o$6" 'B3'
        f=$( printf '%g' "$( dc -e "$_scale k$3 $4$1p" )" )
        s=$( printf '%g' "$( dc -e "$_scale k$( norm_int "$f" ) $5$2p" )" )
        test "$o" = '/' && test "$s" = 0 && continue
        dc -e "$_scale k$( norm_int "$s" ) $6$o p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$3 $1 $4" "$f" "$f $2 $5" "$s" "$s $o $6" 'B3' || break 9
    done
}

# type: Left sided parentheses 1
# form: (A  x  B)  y  C  z   D
# args: ($3 $1 $4) $2 $5 $o $6
# example: (1-2)*3*8
explore_l1() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$o" = '/' && test "$6" -eq 0 && continue # shouldn't happen
        test "$2" = '/' && test "$5" -eq 0 && continue # shouldn't happen
        show_debug "($3$1$4)$2$5$o$6" 'L1'
        f=$( printf '%g' "$( dc -e "$_scale k$3 $4$1p" )" )
        s=$( printf '%g' "$( dc -e "$_scale k$( norm_int "$f" ) $5$2p" )" )
        dc -e "$_scale k$( norm_int "$s" ) $6$o p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$3 $1 $4" "$f" "$f $2 $5" "$s" "$s $o $6" 'L1' || break 9
    done
}

# type: Left sided parentheses 2
# form: (A  x  B  y  C)  z   D
# args: ($3 $1 $4 $2 $5) $o $6
# example: (8+3+1)*2
explore_l2() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    test "$2" = '/' && test "$5" -eq 0 && return # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$o" = '/' && test "$6" -eq 0 && continue # shouldn't happen
        show_debug "($3$1$4$2$5)$o$6" 'L2'
        f=$( printf '%g' "$( dc -e "$_scale k$3 $4$1p" )" )
        s=$( printf '%g' "$( dc -e "$_scale k$( norm_int "$f" ) $5$2p" )" )
        dc -e "$_scale k$( norm_int "$s" ) $6$o p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$3 $1 $4" "$f" "$f $2 $5" "$s" "$s $o $6" 'L2' || break 9
    done
}

# type: Right sided parentheses 1
# form: A  x  B  y  (C  z  D)
# args: $3 $1 $4 $2 ($5 $o $6)
# example: 1+3*(2-8)
explore_r1() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$o" = '/' && test "$6" -eq 0 && continue # shouldn't happen
        f=$( dc -e "$_scale k$5 $6$o p" 2>/dev/null )
        test "$2" = '/' && test "${f:-0}" = 0 && continue
        show_debug "$3$1$4$2($5$o$6)" 'R1'
        s=$( printf '%g' "$( dc -e "$_scale k$3 $4$1p" )" )
        dc -e "$_scale k$( norm_int "$s" ) $( norm_int "$f" )$2p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$3 $1 $4" "$s" "$5 $o $6" "$f" "$s $2 $(printf '%g' $f)" 'R1' || break 9
    done
}

# type: Right sided parentheses 2
# form: A  x  (B  y  C  z   D)
# args: $3 $1 ($4 $2 $5 $o $6)
# example: 2*(8+1+3)
explore_r2() {
    test "$1" = '/' && test "$4" -eq 0 && return # shouldn't happen
    test "$2" = '/' && test "$5" -eq 0 && continue # shouldn't happen
    local f=0
    local s=0
    for o in '+' '-' '*' '/'
    do
        test "$o" = '/' && test "$6" -eq 0 && continue # shouldn't happen
        f=$( printf '%g' "$( dc -e "$_scale k$4 $5$2p" )" )
        s=$( dc -e "$_scale k$( norm_int "$f" ) $6$o p" 2>/dev/null )
        test "$1" = '/' && test "${s:-0}" = 0 && continue
        show_debug "$3$1($4$2$5$o$6)" 'R2'
        dc -e "$_scale k$3 $( norm_int "$s" )$1p" |
            grep -Eqs "^($_value|$_value\\.0{$_scale})\$" || continue
        show_result "$4 $2 $5" "$f" "$f $o $6" "$s" "$3 $1 $(printf '%g' $s)" 'R2' || break 9
    done
}

_perms=',' # string for internal use
# generate all 24 permutations
for i1 in 1 2 3 4
do
    for i2 in 1 2 3 4
    do
        test $i2 -eq $i1 && continue
        for i3 in 1 2 3 4
        do
            test $i3 -eq $i1 && continue
            test $i3 -eq $i2 && continue
            for i4 in 1 2 3 4
            do
                test $i4 -eq $i1 && continue
                test $i4 -eq $i2 && continue
                test $i4 -eq $i3 && continue
                # explore branche(s) for this permutation
                for o1 in '+' '-' '*' '/'
                do
                    for o2 in '+' '-' '*' '/'
                    do
                        if test $(( _uniqs )) -ge 2
                        then
                            _exprs="${!i1}$o1${!i2}$o2${!i3}???${!i4}"
                            echo "$_perms" |
                                grep -Fqs ",$_exprs," &&
                                continue
                            _perms="$_perms$_exprs,"
                        fi
                        for b in $( echo "$_explo" | tr 'BLR,' 'blr ' )
                        do
                            if test $(( _uniqs )) -eq 1
                            then
                                _exprs="${!i1}$o1${!i2}$o2${!i3}$b?${!i4}"
                                echo "$_perms" |
                                    grep -Fqs ",$_exprs," &&
                                    continue
                                _perms="$_perms$_exprs,"
                            fi
                            "explore_$b" "$o1" "$o2" \
                                "${!i1}" "${!i2}" "${!i3}" "${!i4}"
                        done
                    done
                done
            done
        done
    done
done

printf "Found $_found %s over $_check computations.\n" \
    "$( test $(( _found )) -le 1 && echo 'solution' || echo 'solutions' )"
# EOF
